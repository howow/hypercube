'use strict';

const router = new require('koa-router')({
    prefix: '/api'
});

const demo = require('../lib/demo');

router.use('/demo', demo.routes());

module.exports = router;