'use strict';

const router = new require('koa-router')();

const echo = async function(ctx, next) {
    ctx.body = `pong ${ctx.echo || ''} - ,
		ips: ${ctx.ips},
		ip: ${ctx.ip},
		src::${ctx.req.socket.remoteAddress}`;
}


router
    .param('oid', (id, ctx, next) => {
        ctx.echo = '[' + id + ']';
        return next();
    })
    .post('/ping', (ctx)=> {
	    ctx.echo = `[ ${JSON.stringify(ctx.request.body)} ]`;
	    echo(ctx);
    })
    .get('/ping', echo)
    .get('/ping/:oid', echo);


module.exports = router;
