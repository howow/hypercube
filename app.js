'use strict';

const Koa = require('koa');
const app = new Koa();
app.proxy = true;

// static 
const serve = require('koa-static')
app.use(serve('public'));

// support post
const parseBody = require('koa-body');
app.use(parseBody());

// log
const Logger = require('koa-logger');
app.use(Logger());

// routes
const router = require('./routes/index')
app.use(router.routes(), router.allowedMethods());


// server start
function normalizePort(val) {
    var port = +val;
    return isNaN(port) ? val : (port >= 0 ? port : false);
}

const port = normalizePort(process.env.PORT || '8543');
app.listen(port);

