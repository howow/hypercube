module.exports = {
  apps: [{
    env: {
      PORT: 8091,
      NODE_ENV: 'development'
    },
    env_test: {
      PORT: 8091,
      NODE_ENV: 'test',
      watch: false
    },
    env_production: {
      PORT: 8091,
      NODE_ENV: 'production',
      watch: false
    },
    log_date_format: 'MM-DD HH:mm.ss.SSS',
    watch: [
      'app.js',
      'etc',
      'lib',
      'routes'
    ],
    script: 'app.js',
    name: 'hypercube'
  }]
}
